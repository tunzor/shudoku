package com.tunzor.shudoku;

import android.util.Log;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Anthony on 2/14/2017.
 */

public class Board {
    private List<String> boardNumbers;
    private List<String> boardPuzzleNumbers;
    private String[] emptyBoard = new String[] {
            "?","?","?","?","?","?","?","?","?",
            "?","?","?","?","?","?","?","?","?",
            "?","?","?","?","?","?","?","?","?",
            "?","?","?","?","?","?","?","?","?",
            "?","?","?","?","?","?","?","?","?",
            "?","?","?","?","?","?","?","?","?",
            "?","?","?","?","?","?","?","?","?",
            "?","?","?","?","?","?","?","?","?",
            "?","?","?","?","?","?","?","?","?"
    };
    private String[] board1 = new String[] {
            "?","4","?","?","?","8","?","7","?",
            "6","?","?","?","1","2","?","4","?",
            "?","?","8","?","5","?","?","?","?",
            "?","?","?","6","9","?","?","?","?",
            "?","6","?","?","8","?","?","?","?",
            "9","7","?","5","2","3","?","1","?",
            "?","?","6","1","4","?","?","?","2",
            "2","?","5","?","?","6","?","3","4",
            "4","?","?","?","?","?","7","?","9"
    };
    private final Integer[] ROW_1 = new Integer[]{0,1,2,3,4,5,6,7,8};
    private final Integer[] ROW_2 = new Integer[]{9,10,11,12,13,14,15,16,17};
    private final Integer[] ROW_3 = new Integer[]{18,19,20,21,22,23,24,25,26};
    private final Integer[] ROW_4 = new Integer[]{27,28,29,30,31,32,33,34,35};
    private final Integer[] ROW_5 = new Integer[]{36,37,38,39,40,41,42,43,44};
    private final Integer[] ROW_6 = new Integer[]{45,46,47,48,49,50,51,52,53};
    private final Integer[] ROW_7 = new Integer[]{54,55,56,57,58,59,60,61,62};
    private final Integer[] ROW_8 = new Integer[]{63,64,65,66,67,68,69,70,71};
    private final Integer[] ROW_9 = new Integer[]{72,73,74,75,76,77,78,79,80};

    private final Integer[] COL_1 = new Integer[]{0,9,18,27,36,45,54,63,72};
    private final Integer[] COL_2 = new Integer[]{1,10,19,28,37,46,55,64,73};
    private final Integer[] COL_3 = new Integer[]{2,11,20,29,38,47,56,65,74};
    private final Integer[] COL_4 = new Integer[]{3,12,21,30,39,48,57,66,75};
    private final Integer[] COL_5 = new Integer[]{4,13,22,31,40,49,58,67,76};
    private final Integer[] COL_6 = new Integer[]{5,14,23,32,41,50,59,68,77};
    private final Integer[] COL_7 = new Integer[]{6,15,24,33,42,51,60,69,78};
    private final Integer[] COL_8 = new Integer[]{7,16,25,34,43,52,61,70,79};
    private final Integer[] COL_9 = new Integer[]{8,17,26,35,44,53,62,71,80};

    private final Integer[] GRID_1 = new Integer[]{0,1,2,9,10,11,18,19,20};
    private final Integer[] GRID_2 = new Integer[]{3,4,5,12,13,14,21,22,23};
    private final Integer[] GRID_3 = new Integer[]{6,7,8,15,16,17,24,25,26};
    private final Integer[] GRID_4 = new Integer[]{27,28,29,36,37,38,45,46,47};
    private final Integer[] GRID_5 = new Integer[]{30,31,32,39,40,41,48,49,50};
    private final Integer[] GRID_6 = new Integer[]{33,34,35,42,43,44,51,52,53};
    private final Integer[] GRID_7 = new Integer[]{54,55,56,63,64,65,72,73,74};
    private final Integer[] GRID_8 = new Integer[]{57,58,59,66,67,68,75,76,77};
    private final Integer[] GRID_9 = new Integer[]{60,61,62,69,70,71,78,79,80};

    public Board() {
        this.boardNumbers = Arrays.asList(emptyBoard);
    }

    public Board(int boardNo) {
        if(boardNo == 1) {
            this.boardNumbers = Arrays.asList(board1);
            this.boardPuzzleNumbers = Arrays.asList(board1);
        }
    }

    public Board(String[] array) {
        this.boardNumbers = Arrays.asList(array);
    }

    public String[] getBoardAsArray() {
        String[] tempArray = new String[81];
        for(int i = 0; i < boardNumbers.size(); i++) {
            tempArray[i] = boardNumbers.get(i);
        }
        return tempArray;
    }

    public String[] getBoardAsArrayWithSpaces() { //Used to set grid with empty cells instead of stored ?
        String[] tempArray = new String[81];
        for(int i = 0; i < boardNumbers.size(); i++) {
            if("?".equalsIgnoreCase(boardNumbers.get(i))) {
                tempArray[i] = " ";
            } else {
                tempArray[i] = boardNumbers.get(i);
            }
        }
        return tempArray;
    }

    public String[] getPuzzleBoardAsArray() {
        String[] tempArray = new String[81];
        for(int i = 0; i < boardPuzzleNumbers.size(); i++) {
            tempArray[i] = boardPuzzleNumbers.get(i);
        }
        return tempArray;
    }

    public String[] getPuzzleBoardAsArrayWithSpaces() { //Used to set grid with empty cells instead of stored ?
        String[] tempArray = new String[81];
        for(int i = 0; i < boardPuzzleNumbers.size(); i++) {
            if("?".equalsIgnoreCase(boardPuzzleNumbers.get(i))) {
                tempArray[i] = " ";
            } else {
                tempArray[i] = boardPuzzleNumbers.get(i);
            }
        }
        return tempArray;
    }

    public void setBoards(String[] currentArray, String[] puzzleArray) {
        this.boardNumbers = Arrays.asList(currentArray);
        this.boardPuzzleNumbers = Arrays.asList(puzzleArray);
    }

    public void setBoards(String currentBoard, String puzzleBoard) {
        String[] currentArray = new String[81];
        String[] puzzleArray = new String[81];
        for(int i = 0; i < currentBoard.length(); i++) {
            currentArray[i] = String.valueOf(currentBoard.charAt(i));
        }
        for(int i = 0; i < puzzleBoard.length(); i++) {
            puzzleArray[i] = String.valueOf(puzzleBoard.charAt(i));
        }
        this.setBoards(currentArray, puzzleArray);
    }

    public String boardToString() {
        String s = Arrays.toString(getBoardAsArray()).replaceAll(" ", "");
        s = s.replaceAll(",", "");
        s = s.substring(1, s.length()-1);
        return s;
    }

    public String puzzleBoardToString() {
        String s = Arrays.toString(getPuzzleBoardAsArray()).replaceAll(" ", "");
        s = s.replaceAll(",", "");
        s = s.substring(1, s.length()-1);
        return s;
    }

    public String boardToStringForDisplay() { //Returns board with spaces instead of ?
        String s = Arrays.toString(getBoardAsArray()).replaceAll(" ", "");
        s = s.replaceAll(",", "");
        s = s.substring(1, s.length()-1);
        s = s.replaceAll("\\?"," ");
        s = s.replaceAll(" ","\\?");
        return s;
    }

    public void updateCell(int position, String number) {
        boardNumbers.set(position, number);
    }

    public void emptyBoard() {
        this.boardNumbers = Arrays.asList(emptyBoard);
    }

    public Integer[] getRowOfCell(int cell) {
        if(Arrays.asList(ROW_1).indexOf(cell) != -1) {
            return ROW_1;
        } else if(Arrays.asList(ROW_2).indexOf(cell) != -1) {
            return ROW_2;
        } else if(Arrays.asList(ROW_3).indexOf(cell) != -1) {
            return ROW_3;
        } else if(Arrays.asList(ROW_4).indexOf(cell) != -1) {
            return ROW_4;
        } else if(Arrays.asList(ROW_5).indexOf(cell) != -1) {
            return ROW_5;
        } else if(Arrays.asList(ROW_6).indexOf(cell) != -1) {
            return ROW_6;
        } else if(Arrays.asList(ROW_7).indexOf(cell) != -1) {
            return ROW_7;
        } else if(Arrays.asList(ROW_8).indexOf(cell) != -1) {
            return ROW_8;
        } else {
            return ROW_9;
        }
    }

    public Integer[] getColOfCell(int cell) {
        if(Arrays.asList(COL_1).indexOf(cell) != -1) {
            return COL_1;
        } else if(Arrays.asList(COL_2).indexOf(cell) != -1) {
            return COL_2;
        } else if(Arrays.asList(COL_3).indexOf(cell) != -1) {
            return COL_3;
        } else if(Arrays.asList(COL_4).indexOf(cell) != -1) {
            return COL_4;
        } else if(Arrays.asList(COL_5).indexOf(cell) != -1) {
            return COL_5;
        } else if(Arrays.asList(COL_6).indexOf(cell) != -1) {
            return COL_6;
        } else if(Arrays.asList(COL_7).indexOf(cell) != -1) {
            return COL_7;
        } else if(Arrays.asList(COL_8).indexOf(cell) != -1) {
            return COL_8;
        } else {
            return COL_9;
        }
    }

    public Integer[] getGridOfCell(int cell) {
        if(Arrays.asList(GRID_1).indexOf(cell) != -1) {
            return GRID_1;
        } else if(Arrays.asList(GRID_2).indexOf(cell) != -1) {
            return GRID_2;
        } else if(Arrays.asList(GRID_3).indexOf(cell) != -1) {
            return GRID_3;
        } else if(Arrays.asList(GRID_4).indexOf(cell) != -1) {
            return GRID_4;
        } else if(Arrays.asList(GRID_5).indexOf(cell) != -1) {
            return GRID_5;
        } else if(Arrays.asList(GRID_6).indexOf(cell) != -1) {
            return GRID_6;
        } else if(Arrays.asList(GRID_7).indexOf(cell) != -1) {
            return GRID_7;
        } else if(Arrays.asList(GRID_8).indexOf(cell) != -1) {
            return GRID_8;
        } else {
            return GRID_9;
        }
    }

    public boolean isNumberCollision(int num, int cell) {
        Integer[] row = getRowOfCell(cell);
        Integer[] col = getColOfCell(cell);
        Integer[] grid = getGridOfCell(cell);

        for(int i = 0; i < row.length; i++) {
            int puzzleCellVal, gridCellVal;
            if(boardPuzzleNumbers.get(row[i]).equalsIgnoreCase("?")) {
                puzzleCellVal = 0;
            } else {
                puzzleCellVal = Integer.parseInt(boardPuzzleNumbers.get(row[i]));
            }
            if(boardNumbers.get(row[i]).equalsIgnoreCase("?")) {
                gridCellVal = 0;
            } else {
                gridCellVal = Integer.parseInt(boardNumbers.get(row[i]));
            }
            if((puzzleCellVal == num || gridCellVal == num) && row[i] != cell) {
                return true;
            }
        }

        for(int i = 0; i < col.length; i++) {
            int puzzleCellVal, gridCellVal;
            if(boardPuzzleNumbers.get(col[i]).equalsIgnoreCase("?")) {
                puzzleCellVal = 0;
            } else {
                puzzleCellVal = Integer.parseInt(boardPuzzleNumbers.get(col[i]));
            }
            if(boardNumbers.get(col[i]).equalsIgnoreCase("?")) {
                gridCellVal = 0;
            } else {
                gridCellVal = Integer.parseInt(boardNumbers.get(col[i]));
            }
            if((puzzleCellVal == num || gridCellVal == num) && col[i] != cell) {
                return true;
            }
        }

        for(int i = 0; i < grid.length; i++) {
            int puzzleCellVal, gridCellVal;
            if(boardPuzzleNumbers.get(grid[i]).equalsIgnoreCase("?")) {
                puzzleCellVal = 0;
            } else {
                puzzleCellVal = Integer.parseInt(boardPuzzleNumbers.get(grid[i]));
            }
            if(boardNumbers.get(grid[i]).equalsIgnoreCase("?")) {
                gridCellVal = 0;
            } else {
                gridCellVal = Integer.parseInt(boardNumbers.get(grid[i]));
            }
            if((puzzleCellVal == num || gridCellVal == num) && grid[i] != cell) {
                return true;
            }
        }
        return false;
    }
}
