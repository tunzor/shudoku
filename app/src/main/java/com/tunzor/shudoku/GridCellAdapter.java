package com.tunzor.shudoku;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by Anthony on 2/20/2017.
 */

public class GridCellAdapter extends BaseAdapter {

    private Board board;
    private Context thisContext;
    private String[] currentGrid;
    private String[] puzzleGrid;

    public GridCellAdapter(Context context, String[] puzzleGrid) {
        this.thisContext = context;
        this.puzzleGrid = puzzleGrid;
        this.currentGrid = null;
    }

    public GridCellAdapter(Context context, String[] currentGrid, String[] puzzleGrid) {
        this.thisContext = context;
        this.puzzleGrid = puzzleGrid;
        this.currentGrid = currentGrid;
    }

    public GridCellAdapter(Context context, String[] currentGrid, String[] puzzleGrid, Board board) {
        this.thisContext = context;
        this.puzzleGrid = puzzleGrid;
        this.currentGrid = currentGrid;
        this.board = board;
    }

    @Override
    public int getCount() {
        return 81;
    }

    @Override
    public Object getItem(int position) {
        return currentGrid[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view;

        if(convertView == null) {
            view = (TextView) LayoutInflater.from(thisContext).inflate(R.layout.grid_number, parent, false);
        } else {
            view = (TextView) convertView;
        }

        if(puzzleGrid != null) {
            if (puzzleGrid[position].equalsIgnoreCase(" ")) {
                if (currentGrid[position].equalsIgnoreCase(" ")) {
                    view.setText(" ");
                } else {
                    view.setText(currentGrid[position]);
                    if(board != null) {
                        if (board.isNumberCollision(Integer.parseInt(currentGrid[position]), position)) {
                            view.setTextColor(Color.parseColor("#FF0000"));
                        }
                    }
                }
                if(hasBorderRightOfCell(position) ) {
                    view.setBackgroundResource(R.drawable.grid_background_nonpuzzle_border_right);
                } else if(hasBorderBottomOfCell(position) ) {
                    view.setBackgroundResource(R.drawable.grid_background_nonpuzzle_border_bottom);
                } else if(hasBorderBottomAndRightOfCell(position)) {
                    view.setBackgroundResource(R.drawable.grid_background_nonpuzzle_border_bottom_and_right);
                } else {
                    view.setBackgroundResource(R.drawable.grid_background);
                }
                view.setClickable(false);
            } else {
                view.setText(puzzleGrid[position]);
                view.setClickable(true);
                if(hasBorderRightOfCell(position) ) {
                    view.setBackgroundResource(R.drawable.grid_background_puzzle_border_right);
                } else if(hasBorderBottomOfCell(position) ) {
                    view.setBackgroundResource(R.drawable.grid_background_puzzle_border_bottom);
                } else if(hasBorderBottomAndRightOfCell(position)) {
                    view.setBackgroundResource(R.drawable.grid_background_puzzle_border_bottom_and_right);
                } else {
                    view.setBackgroundResource(R.drawable.grid_background_puzzle);
                }
            }
        }
        return view;
    }

    private boolean hasBorderRightOfCell(int pos) {
        return pos == 5 || pos == 14 || pos == 32 || pos == 41 || pos == 59 ||
                pos == 68 || pos == 77 ||
        pos == 2 || pos == 11 || pos == 29 || pos == 38 || pos == 56 ||
                pos == 65 || pos == 74;
    }
    private boolean hasBorderBottomOfCell(int pos) {
        return pos == 18 || pos == 19 || pos == 21 || pos == 22 || pos == 24 ||
                pos == 25 || pos == 26 ||
                pos == 45 || pos == 46 ||pos == 48 || pos == 49 || pos == 51 ||
                pos == 52 || pos == 53;
    }
    private boolean hasBorderBottomAndRightOfCell(int pos) {
        return pos == 20 || pos == 23 || pos == 47 || pos == 50;
    }
}
