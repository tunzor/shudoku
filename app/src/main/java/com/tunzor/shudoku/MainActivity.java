package com.tunzor.shudoku;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    final String SHARED_PREF_CURRENT_BOARD_KEY = "currentGrid";
    final String SHARED_PREF_PUZZLE_BOARD_KEY = "puzzleGrid";
    final String EMPTY_BOARD = "?????????????????????????????????????????????????????????????????????????????????";
    GridView grid;
    Board board;
    TextView feedback, selectedCell, gridOutputFeedback;
    int selectedCellId;
    String[] gridOutput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("onCreate", "onCreate called");
        getBoardFromSharedPref();
        saveBoardToSharedPref();
        requestWindowFeature(Window.FEATURE_NO_TITLE);//hides title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//hides notification bar
        setContentView(R.layout.activity_main);

        grid = (GridView) findViewById(R.id.board_grid_view);
        feedback = (TextView) findViewById(R.id.board_tap_feedback);
        gridOutputFeedback = (TextView) findViewById(R.id.board_grid_output_feedback);

        GridCellAdapter adapter = new GridCellAdapter(this, board.getBoardAsArrayWithSpaces(), board.getPuzzleBoardAsArrayWithSpaces(), board);

        grid.setAdapter(adapter);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = "You selected the number at position "+position;

                feedback.setText(text);
                if(selectedCell != null) {
                    if(hasBorderRightOfCell(selectedCellId)) {
                        selectedCell.setBackgroundResource(R.drawable.grid_background_nonpuzzle_border_right);
                    } else if(hasBorderBottomOfCell(selectedCellId)) {
                        selectedCell.setBackgroundResource(R.drawable.grid_background_nonpuzzle_border_bottom);
                    } else if(hasBorderBottomAndRightOfCell(selectedCellId)) {
                        selectedCell.setBackgroundResource(R.drawable.grid_background_nonpuzzle_border_bottom_and_right);
                    } else {
                        selectedCell.setBackgroundResource(R.drawable.grid_background);
                    }
                }
                selectedCell = (TextView) view;
                selectedCellId = position;
                selectedCell.setBackgroundColor(Color.parseColor("#FFD16E"));
            }
        });
    }

    @Override
    public void onPause(){
        super.onPause();
        Log.d("onPause", "onPause called. "+Arrays.toString(gridOutput));
        saveBoardToSharedPref();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("onResume", "onResume called. "+Arrays.toString(gridOutput));
        getBoardFromSharedPref();
    }

    public void placeNumber(View v) { //Used by 1-9 buttons
        Button b = (Button) v;
        String buttonVal = b.getText().toString().trim();
        if(selectedCell != null) {
            board.updateCell(selectedCellId, b.getText().toString().trim());
            selectedCell.setText(b.getText());
            if(board.isNumberCollision(Integer.parseInt(buttonVal),selectedCellId)) {
                selectedCell.setTextColor(Color.parseColor("#FF0000"));
            } else {
                selectedCell.setTextColor(Color.parseColor("#000000"));
            }
        }
    }

    public void outputGrid(View v) {//Outputs grid as string to screen view
        gridOutputFeedback.setText(board.boardToStringForDisplay());
    }

    public void clearCell(View v) {
        if(selectedCell != null) {
            board.updateCell(selectedCellId, "?");
            selectedCell.setText("");
        }
    }

    public void saveToPrefs(View v) {
        gridOutputFeedback.setText(board.boardToString());
        if(saveBoardToSharedPref()) {
            Toast.makeText(this,"Board saved to shared preferences.",Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this,"Board has not saved to shared preferences.",Toast.LENGTH_SHORT).show();
        }
    }

    public void clearGridFromPrefs(View v) {
        SharedPreferences prefs = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(SHARED_PREF_CURRENT_BOARD_KEY);
        editor.apply();
        board.emptyBoard();
        grid.setAdapter(new GridCellAdapter(this, board.getBoardAsArrayWithSpaces(), board.getPuzzleBoardAsArrayWithSpaces()));
        Toast.makeText(this,"Board removed from shared preferences.",Toast.LENGTH_SHORT).show();
    }

    public boolean saveBoardToSharedPref() {
        SharedPreferences prefs = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Log.d("saveCurrentSharedPref", board.boardToString());
        Log.d("savePuzzleSharedPref", board.puzzleBoardToString());
        editor.remove(SHARED_PREF_CURRENT_BOARD_KEY);
        editor.remove(SHARED_PREF_PUZZLE_BOARD_KEY);
        editor.putString(SHARED_PREF_CURRENT_BOARD_KEY, board.boardToString());
        editor.putString(SHARED_PREF_PUZZLE_BOARD_KEY, board.puzzleBoardToString());
        return editor.commit();
    }

    public void getBoardFromSharedPref() {
        SharedPreferences prefs = this.getPreferences(Context.MODE_PRIVATE);
        String currentBoard = prefs.getString(SHARED_PREF_CURRENT_BOARD_KEY, "");
        String puzzleBoard = prefs.getString(SHARED_PREF_PUZZLE_BOARD_KEY, "");
        if(currentBoard.equalsIgnoreCase("")) {
            board = new Board(1);
        } else {
            Log.d("currentBoardFromPref", currentBoard);
            Log.d("puzzleBoardFromPref", puzzleBoard);
            board = new Board();
            board.setBoards(currentBoard, puzzleBoard);
        }
    }

    private boolean hasBorderRightOfCell(int pos) {
        return pos == 5 || pos == 14 || pos == 32 || pos == 41 || pos == 59 ||
                pos == 68 || pos == 77 ||
                pos == 2 || pos == 11 || pos == 29 || pos == 38 || pos == 56 ||
                pos == 65 || pos == 74;
    }
    private boolean hasBorderBottomOfCell(int pos) {
        return pos == 18 || pos == 19 || pos == 21 || pos == 22 || pos == 24 ||
                pos == 25 || pos == 26 ||
                pos == 45 || pos == 46 ||pos == 48 || pos == 49 || pos == 51 ||
                pos == 52 || pos == 53;
    }
    private boolean hasBorderBottomAndRightOfCell(int pos) {
        return pos == 20 || pos == 23 || pos == 47 || pos == 50;
    }
}
