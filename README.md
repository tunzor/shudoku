# Shudoku #
A sudoku game that uses shapes and colours instead of numbers.

## Learning Experiences ##
In developing this project, I learned about and used these:

* SharedPreferences and saving key-value pairs to persist data across sessions

* GridView and dynamic population through code

* Using onPause and onResume activity methods to automatically save and load data